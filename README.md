REST Sample project. This project includes technology stack using in 05Media Production:
- jersey 1.18.1
- spring 4.0.3
- mongo 2.12.2
- postman plugin for testing REST api.

Steps to follow for running project:
- Start Mongodb on local machine 
- Import Maven project in IntelliJ IDEA.
- If configuration is not already created, create configuration in IntelliJ. 
    3.1 Edit Configurations -> Add new Configuration -> Maven
    3.2 On Command line type: clean install tomcat7:run
    3.3 On Profiles type: localTomcat
- Apply, OK
- Run project


