REST Sample project. This project includes technology stack using in 05Media Production:
- jersey 1.18.1
- spring 4.0.3
- mongo 2.12.2
- postman plugin for testing REST api.

Steps to follow for running project:
1) Start Mongodb on local machine 
2) Import Maven project in IntelliJ IDEA.
3) If configuration is not already created, create configuration in IntelliJ. 
    3.1 Edit Configurations -> Add new Configuration -> Maven
    3.2 On Command line type: clean install tomcat7:run
    3.3 On Profiles type: localTomcat
4) Apply, OK
5) Run project

Obs: For running project from terminal you must have Maven installed. Command: clean install tomcat7:run
