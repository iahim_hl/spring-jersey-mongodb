package com.linkscreens.sample.javax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 */
@Component
@Path("/test")
public class TestApp {
    /**
     * Log class activity
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TestApp.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response testApp(@QueryParam("name") String name) {
        LOGGER.debug("Test app created");

        Map<String, String> message = new HashMap<String, String>();
        message.put("message", "Hello " + name + " from Rest Sample Project");

        return Response.status(Response.Status.OK).entity(message).build();
    }
}
