package com.linkscreens.sample.javax;

import com.linkscreens.sample.dao.entity.UserDetail;
import com.linkscreens.sample.service.UserDetailService;
import com.linkscreens.sample.service.exceptions.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 */
@Component
@Path("/users")
public class UserDetailWS {
    /**
     * Log class activity
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailWS.class);

    @Autowired
    private UserDetailService userDetailService;

    /**
     * Save new user in database
     * @param userDetail - user detail body
     * @return
     * @throws AppException
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveUserDetail(UserDetail userDetail) throws AppException {
        LOGGER.debug("Add new user");
        /*save new user*/
        userDetailService.saveUserDetail(userDetail);

        return Response.status(Response.Status.CREATED).build();
    }

    /**
     * Get user by userId
     * @param userId
     * @return
     * @throws AppException
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public Response getUserDetail(@PathParam("userId") String userId) throws AppException {
        LOGGER.debug("Get user with id: " + userId);
        /*get user detail*/
        return Response.status(Response.Status.OK).entity(userDetailService.getUserById(userId)).build();
    }

    /**
     * Delete user by id
     * @param userId
     * @return
     * @throws AppException
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public Response deleteUserDetail(@PathParam("userId") String userId) throws AppException {
        LOGGER.debug("Delete user with id: " + userId);

        /*delete user*/
        userDetailService.deleteUser(userId);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    /**
     * Update user information
     * @param userId - userId saved in database
     * @param newInformation - new information provided by client
     * @return
     * @throws AppException
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public Response updateUserDetail(@PathParam("userId") String userId, UserDetail newInformation) throws AppException {
        LOGGER.debug("Update user details for userId: " + userId);
        /*update user detail information*/
        return Response.status(Response.Status.OK).entity(userDetailService.updateUser(userId, newInformation)).build();
    }
}
