package com.linkscreens.sample.dao.constants;

import javax.ws.rs.core.Response;

/**
 * Created by evo on 7/7/2016.
 */
public enum ErrorType {// HTTP STATUS CODES (2xx)
    OK(Response.Status.OK), // 200
    NO_CONTENT(Response.Status.NO_CONTENT), // 204

    // HTTP STATUS CODES (3XX)
    NOT_MODIFIED(Response.Status.NOT_MODIFIED), // 304

    // HTTP STATUS CODES (4XX)
    BAD_REQUEST(Response.Status.BAD_REQUEST), // 400
    UNAUTHORIZED(Response.Status.UNAUTHORIZED), // 401
    FORBIDDEN(Response.Status.FORBIDDEN), // 403
    NOT_FOUND(Response.Status.NOT_FOUND), // 404
    CONFLICT(Response.Status.CONFLICT), // 409 victor.badea 16.10.2013

    // HTTP STATUS CODES (5XX)
    SERVER_ERROR(Response.Status.INTERNAL_SERVER_ERROR), // 500
    SERVICE_UNAVAILABLE(Response.Status.SERVICE_UNAVAILABLE); // 503

    // / HTTP status code
    private Response.Status status;

    private ErrorType(Response.Status status) {
        this.status = status;
    }

    public Response.Status getStatus() {
        return status;
    }
}
