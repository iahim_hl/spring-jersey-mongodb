package com.linkscreens.sample.dao;

import com.linkscreens.sample.dao.entity.UserDetail;
import com.mongodb.MongoClient;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.dao.BasicDAO;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.UpdateOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 * Dao class implementation for UserDetailDao interface
 */
@Repository
public class UserDetailDaoImpl extends BasicDAO<UserDetail, ObjectId> implements UserDetailDao {

    @Autowired
    protected UserDetailDaoImpl(MongoClient mongoClient, Morphia morphia, @Value("#{mongodbconfig['mongo.dbname']}") String dbName) {
        super(mongoClient, morphia, dbName);
    }

    /**
     * Save new user
     *
     * @param userDetail
     * @return - ObjectId from database
     */
    public String saveUserDetail(UserDetail userDetail) {
        return super.save(userDetail).getId().toString();
    }

    /**
     * Get User Detail by id
     *
     * @param userId
     * @return
     */
    public UserDetail getUserById(String userId) {
        return super.get(new ObjectId(userId));
    }

    /**
     * Delete user by id
     *
     * @param userId
     * @return
     */
    public boolean deleteUser(String userId) {
        return super.deleteById(new ObjectId(userId)).getN() > 0;
    }

    /**
     * Update user detail by id
     *
     * @param userId         - userId from database
     * @param newInformation - new User Detail object information
     * @return
     */
    public UserDetail updateUser(String userId, UserDetail newInformation) {
        /*create map condition*/
        Map<String, Object> mapCondition = new HashMap<String, Object>();
        mapCondition.put("_id", new ObjectId(userId));

        /*create map with updated parameters; these information will be saved in database for current user*/
        Map<String, Object> updatedParameters = new HashMap<String, Object>();

        /*start checking updated values*/
        if ((newInformation.getFirstName() != null) && (!newInformation.getFirstName().isEmpty())) {
            updatedParameters.put("firstName", newInformation.getFirstName());
        }

        if ((newInformation.getLastName() != null) && (!newInformation.getLastName().isEmpty())) {
            updatedParameters.put("lastName", newInformation.getLastName());
        }

        if ((newInformation.getPassword() != null) && (!newInformation.getPassword().isEmpty())) {
            updatedParameters.put("password", newInformation.getLastName());
        }

        return findAndModifyNoCreateByQuery(mapCondition, updatedParameters);
    }

    /**
     * Check if there's an user with this email
     *
     * @param email
     * @return
     */
    public boolean existsUserByEmail(String email) {
        Query<UserDetail> query = super.createQuery();
        query.filter("email", email);

        return super.exists(query);
    }

    /**
     * Check if user exist by it's id
     *
     * @param userId
     * @return
     */
    public boolean existsUserById(String userId) {
        Query<UserDetail> query = super.createQuery();
        query.filter("_id", new ObjectId(userId));

        return super.exists(query);
    }

    /**
     * Finds and modifies an object in the database without creating it if it
     * does not exist.
     *
     * @param parameters       the parameters by which to search
     * @param updateParameters the parameters to be updated
     * @return the resulting updated object or null if it does not exist
     * @author victor.badea 13.09.2013
     */
    private UserDetail findAndModifyNoCreateByQuery(Map<String, Object> parameters, Map<String, Object> updateParameters) {
        /*create query*/
        Query<UserDetail> query = super.createQuery();

        /*update parameters and values for filter*/
        addParametersToQuery(query, parameters);

        UpdateOperations<UserDetail> ops = super.createUpdateOperations();

        /*add which parameters to be updated*/
        addUpdateParametersToUpdateOperations(ops, updateParameters);

        /*oldVersion = false; createIfMissing = false*/
        return getDs().findAndModify(query, ops, false, false);
    }

    /**
     * Populate query with a set of parameters
     *
     * @param query
     * @param parameters
     */
    private void addParametersToQuery(Query<UserDetail> query, Map<String, Object> parameters) {
        /*if map is not empty, add the parameters*/
        if ((parameters != null) && (!parameters.isEmpty())) {

            /*iterate through map*/
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                if (entry.getValue() instanceof String) {
                    query.disableValidation().filter(entry.getKey(), entry.getValue()).enableValidation();
                } else {
                    query.filter(entry.getKey(), entry.getValue());
                }
            }
        }
    }

    /**
     * Add parameters to update operation
     *
     * @param updateOperations
     * @param parameters
     */
    private void addUpdateParametersToUpdateOperations(UpdateOperations<UserDetail> updateOperations, Map<String, Object> parameters) {
        /*if the map is not empty, add the parameters*/
        if ((parameters != null) && (!parameters.isEmpty())) {

            /*iterate through the map*/
            for (Map.Entry<String, Object> entry : parameters.entrySet()) {
                if (entry.getValue() == null) {
                    updateOperations.unset(entry.getKey());
                } else {
                    updateOperations.set(entry.getKey(), entry.getValue());
                }
            }
        }
    }
}
