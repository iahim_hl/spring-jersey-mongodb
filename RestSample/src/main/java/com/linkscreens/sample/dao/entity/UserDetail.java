package com.linkscreens.sample.dao.entity;

import org.bson.types.ObjectId;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

/**
 * Created by evo on 7/7/2016.
 */
@Entity(value = "UserDetail", noClassnameStored = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class UserDetail {

    @Id
    private ObjectId id;

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public UserDetail() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonIgnore
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public String getEncriptedPassword() {
        return "****";
    }

    @JsonProperty
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("id")
    public String getId() {
        return ((id != null) ? (id.toString()) : (null));
    }

    @JsonIgnore
    public void setId(ObjectId id) {
        this.id = id;
    }
}
