package com.linkscreens.sample.dao;

import com.linkscreens.sample.dao.entity.UserDetail;

import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 */
public interface UserDetailDao {
    /**
     * Save new user
     * @param userDetail
     * @return - ObjectId from database
     */
    String saveUserDetail(UserDetail userDetail);

    /**
     * Get User Detail by id
     * @param userId
     * @return
     */
    UserDetail getUserById(String userId);

    /**
     * Delete user by id
     * @param userId
     * @return
     */
    boolean deleteUser(String userId);

    /**
     * Update user detail by id
     * @param userId - userId from database
     * @param newInformation
     * @return
     */
    UserDetail updateUser(String userId, UserDetail newInformation);

    /**
     * Check if there's an user with this email
     * @param email
     * @return
     */
    boolean existsUserByEmail(String email);

    /**
     * Check if user exist by it's id
     * @param userId
     * @return
     */
    boolean existsUserById(String userId);
}
