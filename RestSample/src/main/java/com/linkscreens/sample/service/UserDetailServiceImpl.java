package com.linkscreens.sample.service;

import com.linkscreens.sample.dao.constants.ErrorType;
import com.linkscreens.sample.dao.entity.UserDetail;
import com.linkscreens.sample.dao.UserDetailDao;
import com.linkscreens.sample.service.exceptions.AppException;
import com.linkscreens.sample.service.util.UserDetailServiceImplUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 * Service class implementation for UserDetailService interface
 */
@Service
public class UserDetailServiceImpl implements UserDetailService {
    /**
     * Log class activity
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    @Autowired
    private UserDetailDao userDetailDao;

    /**
     * Save new user
     *
     * @param userDetail
     * @return - ObjectId from database
     */
    public String saveUserDetail(UserDetail userDetail) throws AppException {
        LOGGER.debug("Save new user detail");

        /*check user detail validation*/
        UserDetailServiceImplUtil.checkUserDetailValidation(userDetailDao, userDetail);

        /*return id*/
        return userDetailDao.saveUserDetail(userDetail);
    }

    /**
     * Get User Detail by id
     *
     * @param userId
     * @return
     */
    public UserDetail getUserById(String userId) throws AppException {
        LOGGER.debug("Get user with id: " + userId);

        /*get user from database*/
        UserDetail userDetail = userDetailDao.getUserById(userId);
        if (userDetail == null) {
            LOGGER.error("No user found with id: " + userId);
            throw new AppException(ErrorType.NOT_FOUND.getStatus().getStatusCode(), "No user found with id: " + userId, ErrorType.NOT_FOUND);
        }

        /*return user detail*/
        return userDetail;
    }

    /**
     * Delete user by id
     *
     * @param userId
     * @return
     */
    public void deleteUser(String userId) throws AppException {
        LOGGER.debug("Delete user with id: " + userId);

        /*check if user exists in database or not*/
        if (!userDetailDao.existsUserById(userId)) {
            LOGGER.error("No user found with id: " + userId);
            throw new AppException(ErrorType.NOT_FOUND.getStatus().getStatusCode(), "No user found with id: " + userId, ErrorType.NOT_FOUND);
        }

        /*delete user*/
        boolean deleted = userDetailDao.deleteUser(userId);

        if (!deleted) {
            /*there was an error in database*/
            throw new AppException(ErrorType.SERVER_ERROR.getStatus().getStatusCode(), "Error while deleting user with id: " + userId, ErrorType.NOT_FOUND);
        }
    }

    /**
     * Update user detail by id
     *
     * @param userId         - userId from database
     * @param newInformation
     * @return
     */
    public UserDetail updateUser(String userId, UserDetail newInformation) throws AppException {
        /*check if user exists in database*/
        if (!userDetailDao.existsUserById(userId)) {
            LOGGER.error("No user found with id: " + userId);
            throw new AppException(ErrorType.NOT_FOUND.getStatus().getStatusCode(), "No user found with id: " + userId, ErrorType.NOT_FOUND);
        }

        /*return updated user*/
        return userDetailDao.updateUser(userId, newInformation);
    }
}
