package com.linkscreens.sample.service.exceptions;

import com.linkscreens.sample.dao.constants.ErrorType;
import org.codehaus.jackson.annotate.JsonIgnore;


/**
 * Created by evo on 7/7/2016.
 * Exception class
 */
public class AppException extends Exception {
    private static final long serialVersionUID = -8999932578270387947L;

    /**
     * contains redundantly the HTTP status of the response sent back to the
     * client in case of error, so that the developer does not have to look into
     * the response headers. If null a default
     */
    Integer status;

    /**
     * Type of error
     */
    @JsonIgnore
     ErrorType errorType;

    /**
     * @param status
     * @param message
     */
    public AppException(int status, String message, ErrorType errorType) {
        super(message);
        this.status = status;
        this.errorType = errorType;
    }

    public AppException() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }
}
