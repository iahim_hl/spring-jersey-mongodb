package com.linkscreens.sample.service.util;

import com.linkscreens.sample.dao.UserDetailDao;
import com.linkscreens.sample.dao.constants.ErrorType;
import com.linkscreens.sample.dao.entity.UserDetail;
import com.linkscreens.sample.service.exceptions.AppException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by evo on 7/7/2016.
 * Validation class
 */
public class UserDetailServiceImplUtil {
    /**
     * Log class activity
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDetailServiceImplUtil.class);

    /**
     * Check user detail information
     * @param userDetailDao
     * @param userDetail
     * @throws AppException
     */
    public static void checkUserDetailValidation(UserDetailDao userDetailDao, UserDetail userDetail) throws AppException {

        if (userDetail == null) {
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "No user detail provided", ErrorType.BAD_REQUEST);
        }

        if ((userDetail.getFirstName() == null) || (userDetail.getFirstName().isEmpty())) {
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "First name can't be null or empty", ErrorType.BAD_REQUEST);
        }

        if ((userDetail.getLastName() == null) || (userDetail.getLastName().isEmpty())) {
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "Last name can't be null or empty", ErrorType.BAD_REQUEST);
        }

        if ((userDetail.getEmail() == null) || (userDetail.getEmail().isEmpty())) {
            //+ niky's validations
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "Email can't be null or empty", ErrorType.BAD_REQUEST);
        }

        if ((userDetail.getPassword() == null) || (userDetail.getPassword().isEmpty())) {
            //+ niky's validations
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "Password can't be null or empty", ErrorType.BAD_REQUEST);
        }

        /*check if user exists*/
        if (userDetailDao.existsUserByEmail(userDetail.getEmail())) {
            LOGGER.warn("User with this email already exists");
            throw new AppException(ErrorType.BAD_REQUEST.getStatus().getStatusCode(), "There's already an user with this email", ErrorType.BAD_REQUEST);
        }
    }
}
