package com.linkscreens.sample.service.exceptions;

import com.linkscreens.sample.dao.constants.ErrorType;
import org.apache.commons.beanutils.BeanUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by evo on 7/7/2016.
 */
public class ErrorMessage {/* contains the same HTTP Status code returned by the server */
    private int status;

    /* message describing the error */
    private String message;

    @JsonIgnore
    /*contains error type*/
    private ErrorType errorType;

    public ErrorMessage() {

    }

    public ErrorMessage(int status, String message, ErrorType errorType) {
        this.status = status;
        this.message = message;
        this.errorType = errorType;
    }

    public ErrorMessage(AppException ex) {
        try {
            BeanUtils.copyProperties(this, ex);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }
}