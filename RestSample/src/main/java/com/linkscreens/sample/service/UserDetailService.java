package com.linkscreens.sample.service;

import com.linkscreens.sample.dao.entity.UserDetail;
import com.linkscreens.sample.service.exceptions.AppException;

import java.util.Map;

/**
 * Created by evo on 7/7/2016.
 */
public interface UserDetailService {
    /**
     * Save new user
     * @param userDetail
     * @return - ObjectId from database
     */
    String saveUserDetail(UserDetail userDetail) throws AppException;

    /**
     * Get User Detail by id
     * @param userId
     * @return
     */
    UserDetail getUserById(String userId) throws AppException;

    /**
     * Delete user by id
     * @param userId
     * @return
     */
    void deleteUser(String userId) throws AppException;

    /**
     * Update user detail by id
     * @param userId - userId from database
     * @param newInformation
     * @return
     */
    UserDetail updateUser(String userId, UserDetail newInformation) throws AppException;
}
